{
  "swagger" : "2.0",
  "info" : {
    "version" : "1.0.0",
    "title" : "API Subscription",
    "contact" : { }
  },
  "host" : "rg01we-dp-dev-app5.westeurope.cloudapp.azure.com",
  "basePath" : "/v1",
  "tags" : [ {
    "name" : "api_subscriptions"
  } ],
  "schemes" : [ "https" ],
  "paths" : {
    "/identity/api_subscriptions" : {
      "get" : {
        "tags" : [ "api_subscriptions" ],
        "summary" : "",
        "description" : "Returns API subscriptions based on the provided filter.",
        "operationId" : "getApiSubscriptions",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "filter",
          "in" : "query",
          "description" : "Ability to filter collection to certain values, it uses FIQL syntax. Example: ?filter=name==snapshot",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort",
          "in" : "query",
          "description" : "Ascending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort_desc",
          "in" : "query",
          "description" : "Descending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "Specifies how many results is returned in response. The default value is 50, max allowed is 200. The response can return all results within single call (there is no cursor paging available).",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "cursor",
          "in" : "query",
          "description" : "Starting paging cursor.",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Collection of API subscriptions.",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/ApiSubscription"
              }
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "post" : {
        "tags" : [ "api_subscriptions" ],
        "summary" : "",
        "description" : "Creates a new API subscription.",
        "operationId" : "addApiSubscription",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "body",
          "description" : "The new API subscription.",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/ApiSubscription"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "The API subscription has been created.",
            "schema" : {
              "$ref" : "#/definitions/ApiSubscription"
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    },
    "/identity/api_subscriptions/{api_subscription_id}" : {
      "get" : {
        "tags" : [ "api_subscriptions" ],
        "summary" : "",
        "description" : "Returns an API subscription with the specified UUID.",
        "operationId" : "getApiSubscription",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "api_subscription_id",
          "in" : "path",
          "description" : "UUID of the API subscription.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "API subscription object.",
            "schema" : {
              "$ref" : "#/definitions/ApiSubscription"
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "post" : {
        "tags" : [ "api_subscriptions" ],
        "summary" : "",
        "description" : "Updates an API subscription.",
        "operationId" : "updateApiSubscription",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "api_subscription_id",
          "in" : "path",
          "description" : "UUID of the API subscription.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "If-Match",
          "in" : "header",
          "description" : "The value taken from Etag HTTP header (specifies the version of the object). It is used by server to validate that client has actual version.",
          "required" : true,
          "type" : "string",
          "default" : ""
        }, {
          "in" : "body",
          "name" : "body",
          "description" : "Updated API subscription.",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/ApiSubscriptionUpdate"
          }
        } ],
        "responses" : {
          "204" : {
            "description" : "The API subscription has been updated."
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "delete" : {
        "tags" : [ "api_subscriptions" ],
        "summary" : "",
        "description" : "Deletes an API subscription.",
        "operationId" : "deleteApiSubscription",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "api_subscription_id",
          "in" : "path",
          "description" : "UUID of the API subscription.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "If-Match",
          "in" : "header",
          "description" : "The value taken from Etag HTTP header (specifies the version of the object). It is used by server to validate that client has actual version.",
          "required" : true,
          "type" : "string",
          "default" : ""
        } ],
        "responses" : {
          "204" : {
            "description" : "The API subscription has been deleted."
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    }
  },
  "securityDefinitions" : {
    "api_key" : {
      "type" : "apiKey",
      "name" : "Authorization",
      "in" : "header"
    }
  },
  "definitions" : {
    "ApiSubscription" : {
      "type" : "object",
      "required" : [ "application_version_id", "commercial_subscription_id", "is_active" ],
      "properties" : {
        "is_active" : {
          "type" : "boolean",
          "description" : "Indicates whether the API subscription is active.",
          "default" : false
        },
        "api_subscription_id" : {
          "type" : "string",
          "description" : "UUID of the API subscription."
        },
        "application_version_id" : {
          "type" : "string",
          "description" : "UUID of the subscribing application version."
        },
        "commercial_subscription_id" : {
          "type" : "string",
          "description" : "UUID of the associated commercial subscription."
        }
      }
    },
    "ApiSubscriptionUpdate" : {
      "type" : "object",
      "required" : [ "is_active" ],
      "properties" : {
        "is_active" : {
          "type" : "boolean",
          "description" : "Indicates whether the API subscription is active.",
          "default" : false
        }
      },
      "description" : "Contains only editable fields."
    }
  }
}