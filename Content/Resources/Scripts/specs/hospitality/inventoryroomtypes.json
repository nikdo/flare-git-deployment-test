{
  "swagger" : "2.0",
  "info" : {
    "description" : "TBD\n\n\n",
    "version" : "1.0.0",
    "title" : "Hospitality Configuration - Inventory room type",
    "termsOfService" : "http://developer.snapshot.travel/terms",
    "contact" : {
      "name" : "API team at snapshot.travel",
      "url" : "http://developer.snapshot.travel",
      "email" : "api-team@snapshot.travel"
    }
  },
  "host" : "global.api.snapshot.technology",
  "basePath" : "/v1",
  "tags" : [ {
    "name" : "hospitalityconfiguration",
    "description" : "The Hospitality Entities API"
  } ],
  "schemes" : [ "https" ],
  "paths" : {
    "/hospitality/configuration/inventory_room_types" : {
      "get" : {
        "tags" : [ "hospitalityconfiguration" ],
        "summary" : "",
        "description" : "Returns collection of inventory room types.",
        "operationId" : "getInventoryRoomTypes",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "filter",
          "in" : "query",
          "description" : "Ability to filter collection to certain values, it uses FIQL syntax. Example: ?filter=name==snapshot",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort",
          "in" : "query",
          "description" : "Ascending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort_desc",
          "in" : "query",
          "description" : "Descending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "Specifies how many results is returned in response. The default value is 50, max allowed is 200. The response can return all results within single call (there is no cursor paging available).",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "cursor",
          "in" : "query",
          "description" : "Starting paging cursor.",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Collection containing inventory room types.",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/InventoryItems"
              }
            }
          }
        }
      },
      "post" : {
        "tags" : [ "hospitalityconfiguration" ],
        "summary" : "",
        "description" : "Creates a new inventory room type.",
        "operationId" : "addInventoryRoomType",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "body",
          "description" : "New inventory room type object.",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/InventoryItems"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "$ref" : "#/definitions/Response"
            }
          },
          "201" : {
            "description" : "The created inventory room type object.",
            "schema" : {
              "$ref" : "#/definitions/InventoryRoomTypeCreate"
            }
          }
        }
      }
    },
    "/hospitality/configuration/inventory_room_types/{room_type_inventory_room_type}" : {
      "get" : {
        "tags" : [ "hospitalityconfiguration" ],
        "summary" : "",
        "description" : "Returns a single inventory room type with the specified inventory reference.",
        "operationId" : "getInventoryRoomType",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "room_type_inventory_room_type",
          "in" : "path",
          "description" : "Inventory room type reference (identifier).",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Single inventory room type object.",
            "schema" : {
              "$ref" : "#/definitions/InventoryItems"
            }
          }
        }
      },
      "post" : {
        "tags" : [ "hospitalityconfiguration" ],
        "summary" : "",
        "description" : "Updates an Inventory room type.",
        "operationId" : "updateInventoryRoomType",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "room_type_inventory_room_type",
          "in" : "path",
          "description" : "Inventory room type reference (identifier).",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "If-Match",
          "in" : "header",
          "description" : "The value taken from Etag HTTP header (specifies the version of the object). It is used by server to validate that client has actual version.",
          "required" : true,
          "type" : "string",
          "default" : ""
        }, {
          "in" : "body",
          "name" : "body",
          "description" : "Updated inventory room type object. Include only fields you want to update.",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/InventoryRoomTypeCreate"
          }
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "$ref" : "#/definitions/Response"
            }
          },
          "204" : {
            "description" : "The inventory room type has been updated."
          }
        }
      },
      "delete" : {
        "tags" : [ "hospitalityconfiguration" ],
        "summary" : "",
        "description" : "Deletes an inventory room type.",
        "operationId" : "deleteInventoryRoomType",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "room_type_inventory_room_type",
          "in" : "path",
          "description" : "Inventory room type reference (identifier).",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "successful operation",
            "schema" : {
              "$ref" : "#/definitions/Response"
            }
          },
          "204" : {
            "description" : "Inventory room type has been deleted."
          }
        }
      }
    }
  },
  "definitions" : {
    "DateRange" : {
      "type" : "object",
      "required" : [ "from" ],
      "properties" : {
        "from" : {
          "type" : "string",
          "format" : "date",
          "description" : "Lower limit of given range."
        },
        "to" : {
          "type" : "string",
          "format" : "date",
          "description" : "Upper limit of given range."
        }
      },
      "description" : "Range of dates."
    },
    "InventoryItems" : {
      "type" : "object",
      "properties" : {
        "inventoryRoomTypeItems" : {
          "type" : "array",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/InventoryRoomTypeItem"
          }
        }
      },
      "description" : "Set of a InventoryRoomTypeItem."
    },
    "InventoryOverbokingLimit" : {
      "type" : "object",
      "properties" : {
        "overbookingLimitItems" : {
          "type" : "array",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/OverbookingLimit"
          }
        }
      },
      "description" : "Set of an InventoryRoomTypeItem."
    },
    "InventoryRoomTypeCreate" : {
      "type" : "object",
      "properties" : {
        "inventoryRoomTypeItems" : {
          "type" : "array",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/InventoryRoomTypeItem"
          }
        }
      }
    },
    "InventoryRoomTypeItem" : {
      "type" : "object",
      "required" : [ "room_type_inventory", "room_type_inventory_room_type", "room_type_inventory_start_date" ],
      "properties" : {
        "room_type_inventory_room_type" : {
          "type" : "string",
          "description" : "Reference to the given room type.",
          "readOnly" : true
        },
        "room_type_inventory_start_date" : {
          "type" : "string",
          "format" : "date",
          "description" : "Start date of the inventory.",
          "readOnly" : true
        },
        "room_type_inventory" : {
          "type" : "array",
          "description" : "The actual counter, number of rooms available for the given date.",
          "readOnly" : true,
          "items" : {
            "type" : "integer",
            "format" : "int32"
          }
        },
        "room_type_inventory_overbooking_limit" : {
          "description" : "Overbooking limit of the room type.",
          "readOnly" : true,
          "$ref" : "#/definitions/InventoryOverbokingLimit"
        }
      }
    },
    "OverbookingLimit" : {
      "type" : "object",
      "required" : [ "date_range" ],
      "properties" : {
        "date_range" : {
          "description" : "Data range for which the overbooking rule is valid.",
          "$ref" : "#/definitions/DateRange"
        },
        "number_of_rooms" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Total number of rooms which could be overbooked."
        }
      },
      "description" : "Specifies allowance in overbooking."
    },
    "Property" : {
      "type" : "object",
      "properties" : {
        "position" : {
          "type" : "integer",
          "format" : "int32"
        },
        "format" : {
          "type" : "string"
        },
        "description" : {
          "type" : "string"
        },
        "title" : {
          "type" : "string"
        },
        "vendorExtensions" : {
          "type" : "object",
          "additionalProperties" : {
            "type" : "object"
          }
        },
        "example" : {
          "type" : "string"
        },
        "readOnly" : {
          "type" : "boolean",
          "default" : false
        },
        "xml" : {
          "$ref" : "#/definitions/Xml"
        },
        "type" : {
          "type" : "string"
        }
      }
    },
    "Response" : {
      "type" : "object",
      "properties" : {
        "description" : {
          "type" : "string"
        },
        "schema" : {
          "$ref" : "#/definitions/Property"
        },
        "examples" : {
          "type" : "object",
          "additionalProperties" : {
            "type" : "object"
          }
        },
        "headers" : {
          "type" : "object",
          "additionalProperties" : {
            "$ref" : "#/definitions/Property"
          }
        }
      }
    },
    "Xml" : {
      "type" : "object",
      "properties" : {
        "name" : {
          "type" : "string"
        },
        "namespace" : {
          "type" : "string"
        },
        "prefix" : {
          "type" : "string"
        },
        "attribute" : {
          "type" : "boolean",
          "default" : false
        },
        "wrapped" : {
          "type" : "boolean",
          "default" : false
        }
      }
    }
  }
}