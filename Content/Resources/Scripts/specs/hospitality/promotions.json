{
  "swagger" : "2.0",
  "info" : {
    "version" : "1.0.0",
    "title" : "Hospitality Configuration - Promotion",
    "contact" : { }
  },
  "host" : "rg01we-dp-dev-app5.westeurope.cloudapp.azure.com",
  "basePath" : "/v1",
  "tags" : [ {
    "name" : "promotions"
  } ],
  "schemes" : [ "https" ],
  "paths" : {
    "/hospitality/configuration/promotions" : {
      "get" : {
        "tags" : [ "promotions" ],
        "summary" : "",
        "description" : "Returns a collection of all promotions.",
        "operationId" : "getPromotions",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "filter",
          "in" : "query",
          "description" : "Ability to filter collection to certain values, it uses FIQL syntax. Example: ?filter=name==snapshot",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort",
          "in" : "query",
          "description" : "Ascending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort_desc",
          "in" : "query",
          "description" : "Descending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "Specifies how many results is returned in response. The default value is 50, max allowed is 200. The response can return all results within single call (there is no cursor paging available).",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "cursor",
          "in" : "query",
          "description" : "Starting paging cursor.",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Collection of promotions.",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/Promotion"
              }
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "post" : {
        "tags" : [ "promotions" ],
        "summary" : "",
        "description" : "Creates a new promotion object.",
        "operationId" : "addPromotion",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "body",
          "description" : "New promotion object.",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/PromotionCreate"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "The created promotion object.",
            "schema" : {
              "$ref" : "#/definitions/Promotion"
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    },
    "/hospitality/configuration/promotions/{promotion_id}" : {
      "get" : {
        "tags" : [ "promotions" ],
        "summary" : "",
        "description" : "Returns a single promotion.",
        "operationId" : "getPromotion",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "promotion_id",
          "in" : "path",
          "description" : "Unique identifier of the promotion in UUID format.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Single promotion object.",
            "schema" : {
              "$ref" : "#/definitions/Promotion"
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "post" : {
        "tags" : [ "promotions" ],
        "summary" : "",
        "description" : "Updates a promotion specified by its ID.",
        "operationId" : "updatePromotion",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "promotion_id",
          "in" : "path",
          "description" : "Unique identifier of the promotion in UUID format.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "If-Match",
          "in" : "header",
          "description" : "The value taken from Etag HTTP header (specifies the version of the object). It is used by server to validate that client has actual version.",
          "required" : true,
          "type" : "string",
          "default" : ""
        }, {
          "in" : "body",
          "name" : "body",
          "description" : "Updated promotion object (only fields to be updated).",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/PromotionUpdate"
          }
        } ],
        "responses" : {
          "204" : {
            "description" : "The promotion has been updated."
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "delete" : {
        "tags" : [ "promotions" ],
        "summary" : "",
        "description" : "Deletes a promotion.",
        "operationId" : "deletePromotion",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "promotion_id",
          "in" : "path",
          "description" : "Unique identifier of the promotion in UUID format.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "204" : {
            "description" : "Promotion has been deleted."
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    }
  },
  "securityDefinitions" : {
    "api_key" : {
      "type" : "apiKey",
      "name" : "Authorization",
      "in" : "header"
    }
  },
  "definitions" : {
    "Currency" : {
      "type" : "object",
      "required" : [ "currency", "value" ],
      "properties" : {
        "currency" : {
          "type" : "string",
          "description" : "An ISO 4217 (3) alpha character code that specifies a monetary unit."
        },
        "value" : {
          "type" : "number",
          "format" : "double",
          "description" : "The actual value."
        }
      }
    },
    "DateRange" : {
      "type" : "object",
      "required" : [ "from" ],
      "properties" : {
        "from" : {
          "type" : "string",
          "format" : "date",
          "description" : "Lower limit of given range."
        },
        "to" : {
          "type" : "string",
          "format" : "date",
          "description" : "Upper limit of given range."
        }
      },
      "description" : "Range of dates."
    },
    "LocalizedText" : {
      "type" : "object",
      "required" : [ "language", "text" ],
      "properties" : {
        "language" : {
          "type" : "string",
          "description" : "Language of the text."
        },
        "text" : {
          "type" : "string",
          "description" : "The localized text in the given language."
        }
      },
      "description" : "Localized text allows to localize texts throughout the system. It's an array of the texts with specification of the language."
    },
    "Promotion" : {
      "type" : "object",
      "required" : [ "promotion_code", "promotion_name", "promotion_rate_plan", "promotion_selling_date_range", "promotion_stay_date_range" ],
      "properties" : {
        "promotion_name" : {
          "type" : "array",
          "description" : "Name of the promotion, e.g., \"20% off room price\".",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "promotion_rate_plan" : {
          "description" : "The rate plan to be used when guests use this promotion.",
          "$ref" : "#/definitions/Reference"
        },
        "promotion_description" : {
          "type" : "array",
          "description" : "Description of the promotion.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "promotion_code" : {
          "type" : "string",
          "description" : "Human readable code of the promotion.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "promotion_selling_date_range" : {
          "description" : "Date range, which specifies the time the promotion can be used to book a room.",
          "$ref" : "#/definitions/DateRange"
        },
        "promotion_stay_date_range" : {
          "description" : "Date range, which specifies the dates for which guests can book rooms when using this promotion.",
          "$ref" : "#/definitions/DateRange"
        },
        "promotion_forecast_number_of_bookings" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Forecast number of bookings made using this promotion."
        },
        "promotion_forecast_revenue" : {
          "description" : "Expected revenue from the promotion.",
          "$ref" : "#/definitions/Currency"
        },
        "promotion_forecast_room_nights" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Expected overall number of booked nights with the promotion."
        },
        "promotion_id" : {
          "type" : "string",
          "description" : "Unique ID of the Promotion."
        }
      }
    },
    "PromotionCreate" : {
      "type" : "object",
      "required" : [ "promotion_code", "promotion_name", "promotion_rate_plan", "promotion_selling_date_range", "promotion_stay_date_range" ],
      "properties" : {
        "promotion_name" : {
          "type" : "array",
          "description" : "Name of the promotion, e.g., \"20% off room price\".",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "promotion_rate_plan" : {
          "description" : "The rate plan to be used when guests use this promotion.",
          "$ref" : "#/definitions/Reference"
        },
        "promotion_description" : {
          "type" : "array",
          "description" : "Description of the promotion.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "promotion_code" : {
          "type" : "string",
          "description" : "Human readable code of the promotion.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "promotion_selling_date_range" : {
          "description" : "Date range, which specifies the time the promotion can be used to book a room.",
          "$ref" : "#/definitions/DateRange"
        },
        "promotion_stay_date_range" : {
          "description" : "Date range, which specifies the dates for which guests can book rooms when using this promotion.",
          "$ref" : "#/definitions/DateRange"
        },
        "promotion_forecast_number_of_bookings" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Forecast number of bookings made using this promotion."
        },
        "promotion_forecast_revenue" : {
          "description" : "Expected revenue from the promotion.",
          "$ref" : "#/definitions/Currency"
        },
        "promotion_forecast_room_nights" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Expected overall number of booked nights with the promotion."
        },
        "promotion_id" : {
          "type" : "string",
          "description" : "Unique ID of the Promotion."
        }
      }
    },
    "PromotionUpdate" : {
      "type" : "object",
      "required" : [ "promotion_code", "promotion_name", "promotion_rate_plan", "promotion_selling_date_range", "promotion_stay_date_range" ],
      "properties" : {
        "promotion_name" : {
          "type" : "array",
          "description" : "Name of the promotion, e.g., \"20% off room price\".",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "promotion_rate_plan" : {
          "description" : "The rate plan to be used when guests use this promotion.",
          "$ref" : "#/definitions/Reference"
        },
        "promotion_description" : {
          "type" : "array",
          "description" : "Description of the promotion.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "promotion_code" : {
          "type" : "string",
          "description" : "Human readable code of the promotion.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "promotion_selling_date_range" : {
          "description" : "Date range, which specifies the time the promotion can be used to book a room.",
          "$ref" : "#/definitions/DateRange"
        },
        "promotion_stay_date_range" : {
          "description" : "Date range, which specifies the dates for which guests can book rooms when using this promotion.",
          "$ref" : "#/definitions/DateRange"
        },
        "promotion_forecast_number_of_bookings" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Forecast number of bookings made using this promotion."
        },
        "promotion_forecast_revenue" : {
          "description" : "Expected revenue from the promotion.",
          "$ref" : "#/definitions/Currency"
        },
        "promotion_forecast_room_nights" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Expected overall number of booked nights with the promotion."
        }
      }
    },
    "Reference" : {
      "type" : "object",
      "required" : [ "id", "module" ],
      "properties" : {
        "module" : {
          "type" : "string",
          "description" : "Module name."
        },
        "id" : {
          "type" : "string",
          "description" : "Identifier of referenced entity."
        }
      },
      "description" : "Reference to some other entity (sub-types)."
    }
  }
}