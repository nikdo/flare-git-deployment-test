{
  "swagger" : "2.0",
  "info" : {
    "version" : "1.0.0",
    "title" : "Hospitality Configuration - Customer",
    "contact" : { }
  },
  "host" : "rg01we-dp-dev-app5.westeurope.cloudapp.azure.com",
  "basePath" : "/v1",
  "tags" : [ {
    "name" : "customers"
  } ],
  "schemes" : [ "https" ],
  "paths" : {
    "/hospitality/configuration/customers" : {
      "get" : {
        "tags" : [ "customers" ],
        "summary" : "",
        "description" : "Returns a collection of customers.",
        "operationId" : "getCustomers",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "filter",
          "in" : "query",
          "description" : "Ability to filter collection to certain values, it uses FIQL syntax. Example: ?filter=name==snapshot",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort",
          "in" : "query",
          "description" : "Ascending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort_desc",
          "in" : "query",
          "description" : "Descending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "Specifies how many results is returned in response. The default value is 50, max allowed is 200. The response can return all results within single call (there is no cursor paging available).",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "cursor",
          "in" : "query",
          "description" : "Starting paging cursor.",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Collection containing customers.",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/Customer"
              }
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "post" : {
        "tags" : [ "customers" ],
        "summary" : "",
        "description" : "Creates a new customer.",
        "operationId" : "addCustomer",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "body",
          "description" : "New customer object.",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/CustomerCreate"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "The created customer object.",
            "schema" : {
              "$ref" : "#/definitions/Customer"
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    },
    "/hospitality/configuration/customers/{customer_id}" : {
      "get" : {
        "tags" : [ "customers" ],
        "summary" : "",
        "description" : "Returns a single customer. Searches through both active and inactive customers.",
        "operationId" : "getCustomer",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "customer_id",
          "in" : "path",
          "description" : "Unique identifier of the customer in UUID format.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Single customer object.",
            "schema" : {
              "$ref" : "#/definitions/Customer"
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "post" : {
        "tags" : [ "customers" ],
        "summary" : "",
        "description" : "Updates a customer.",
        "operationId" : "updateCustomer",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "customer_id",
          "in" : "path",
          "description" : "Unique identifier of the customer in UUID format.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "If-Match",
          "in" : "header",
          "description" : "The value taken from Etag HTTP header (specifies the version of the object). It is used by server to validate that client has actual version.",
          "required" : true,
          "type" : "string",
          "default" : ""
        }, {
          "in" : "body",
          "name" : "body",
          "description" : "Updated customer object. Include only fields you want to update.",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/CustomerUpdate"
          }
        } ],
        "responses" : {
          "204" : {
            "description" : "The customer has been updated."
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "delete" : {
        "tags" : [ "customers" ],
        "summary" : "",
        "description" : "Deletes a customer.",
        "operationId" : "deleteCustomer",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "customer_id",
          "in" : "path",
          "description" : "Unique identifier of the customer in UUID format.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "204" : {
            "description" : "Customer has been deleted."
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    },
    "/hospitality/configuration/customers/{customer_id}/child_customers" : {
      "get" : {
        "tags" : [ "customers" ],
        "summary" : "",
        "description" : "Returns a collection of child customers of a specified customer.",
        "operationId" : "getChildCustomers",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "customer_id",
          "in" : "path",
          "description" : "Unique identifier of the customer in UUID format.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "filter",
          "in" : "query",
          "description" : "Ability to filter collection to certain values, it uses FIQL syntax. Example: ?filter=name==snapshot",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort",
          "in" : "query",
          "description" : "Ascending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort_desc",
          "in" : "query",
          "description" : "Descending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "Specifies how many results is returned in response. The default value is 50, max allowed is 200. The response can return all results within single call (there is no cursor paging available).",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "cursor",
          "in" : "query",
          "description" : "Starting paging cursor.",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "The collection of child customers.",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/Customer"
              }
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    },
    "/hospitality/configuration/customers/{customer_id}/properties" : {
      "get" : {
        "tags" : [ "customers" ],
        "summary" : "",
        "description" : "Returns a collection of all properties with which the customer has a relationship. Includes properties where the customer is the anchor customer.",
        "operationId" : "getCustomerProperties",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "customer_id",
          "in" : "path",
          "description" : "Unique identifier of the customer in UUID format.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "filter",
          "in" : "query",
          "description" : "Ability to filter collection to certain values, it uses FIQL syntax. Example: ?filter=name==snapshot",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort",
          "in" : "query",
          "description" : "Ascending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort_desc",
          "in" : "query",
          "description" : "Descending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "Specifies how many results is returned in response. The default value is 50, max allowed is 200. The response can return all results within single call (there is no cursor paging available).",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "cursor",
          "in" : "query",
          "description" : "Starting paging cursor.",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Collection of properties.",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/Property"
              }
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    }
  },
  "securityDefinitions" : {
    "api_key" : {
      "type" : "apiKey",
      "name" : "Authorization",
      "in" : "header"
    }
  },
  "definitions" : {
    "Address" : {
      "type" : "object",
      "required" : [ "address_line1", "city", "country", "is_billing_address", "is_primary_address" ],
      "properties" : {
        "address_line1" : {
          "type" : "string",
          "description" : "First (primary) address line.",
          "minLength" : 0,
          "maxLength" : 500
        },
        "address_line2" : {
          "type" : "string",
          "description" : "Additional address line.",
          "minLength" : 0,
          "maxLength" : 500
        },
        "city" : {
          "type" : "string",
          "description" : "City.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "zip_code" : {
          "type" : "string",
          "description" : "Zip Code.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "country" : {
          "type" : "string",
          "description" : "Two letter country code as defined by ISO 3166."
        },
        "region" : {
          "type" : "string",
          "description" : "Region of the country.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "is_billing_address" : {
          "type" : "boolean",
          "description" : "Specifies whether the address is billing address.",
          "default" : false
        },
        "is_primary_address" : {
          "type" : "boolean",
          "description" : "Specifies that the address is primary.",
          "default" : false
        },
        "address_type" : {
          "type" : "string",
          "description" : "Type of the address.",
          "minLength" : 0,
          "maxLength" : 255
        }
      }
    },
    "AddressUpdate" : {
      "type" : "object",
      "required" : [ "address_line1", "city", "country", "is_billing_address", "is_primary_address" ],
      "properties" : {
        "address_line1" : {
          "type" : "string",
          "description" : "First (primary) address line.",
          "minLength" : 0,
          "maxLength" : 500
        },
        "address_line2" : {
          "type" : "string",
          "description" : "Additional address line.",
          "minLength" : 0,
          "maxLength" : 500
        },
        "city" : {
          "type" : "string",
          "description" : "City.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "zip_code" : {
          "type" : "string",
          "description" : "Zip Code.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "country" : {
          "type" : "string",
          "description" : "Two letter country code as defined by ISO 3166."
        },
        "region" : {
          "type" : "string",
          "description" : "Region of the country.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "is_billing_address" : {
          "type" : "boolean",
          "description" : "Specifies whether the address is billing address.",
          "default" : false
        },
        "is_primary_address" : {
          "type" : "boolean",
          "description" : "Specifies that the address is primary.",
          "default" : false
        },
        "address_type" : {
          "type" : "string",
          "description" : "Type of the address.",
          "minLength" : 0,
          "maxLength" : 255
        }
      },
      "description" : "Contains only editable fields."
    },
    "Attribute" : {
      "type" : "object",
      "required" : [ "attribute_code", "attribute_group", "attribute_name" ],
      "properties" : {
        "attribute_name" : {
          "type" : "array",
          "description" : "The descriptive name of the attribute.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "attribute_code" : {
          "type" : "string",
          "description" : "Code helps programmers work with attributes more easily (e.g., showing an icon for wi-fi attribute).",
          "minLength" : 0,
          "maxLength" : 20
        },
        "attribute_group" : {
          "type" : "string",
          "description" : "Attribute group, e.g. physical, dietary etc.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "attribute_id" : {
          "type" : "string",
          "description" : "Unique ID of the Attribute."
        }
      }
    },
    "ContactInformation" : {
      "type" : "object",
      "required" : [ "contact_information", "format", "is_primary", "language", "medium", "sequence_order", "type" ],
      "properties" : {
        "medium" : {
          "type" : "string",
          "description" : "Type of the medium.",
          "enum" : [ "phone", "mobile_phone", "email", "facebook" ]
        },
        "type" : {
          "type" : "string",
          "description" : "Type of the contact.",
          "enum" : [ "home", "work", "general" ]
        },
        "format" : {
          "type" : "string",
          "description" : "Format of the selected medium.",
          "enum" : [ "html", "text", "voice" ]
        },
        "language" : {
          "type" : "string",
          "description" : "Language of the medium (IETF language tag)."
        },
        "contact_information" : {
          "type" : "string",
          "description" : "Contact information itself, e.g. phone number.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "is_primary" : {
          "type" : "boolean",
          "description" : "Specifies that the address is primary.",
          "default" : false
        },
        "sequence_order" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Order to show. Default order is higher by one from the highest existing (added item goes last).",
          "minimum" : 0.0
        }
      }
    },
    "ContactItem" : {
      "type" : "object",
      "required" : [ "contact_number", "description" ],
      "properties" : {
        "contact_number" : {
          "type" : "string",
          "description" : "Phone number."
        },
        "description" : {
          "type" : "string",
          "description" : "Description of the person on the call (taxi, hospital, ...).",
          "minLength" : 0,
          "maxLength" : 500
        }
      }
    },
    "CustomFields" : {
      "type" : "object",
      "properties" : {
        "customFields" : {
          "type" : "object",
          "additionalProperties" : {
            "type" : "object"
          }
        }
      },
      "description" : "Map of custom fields. Each field is a pair composed of a label and an object."
    },
    "Customer" : {
      "type" : "object",
      "required" : [ "customer_addresses", "customer_code", "customer_communication", "customer_id", "customer_name", "customer_vat_id", "customer_website" ],
      "properties" : {
        "customer_name" : {
          "type" : "string",
          "description" : "International customer name.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_alternative_name" : {
          "type" : "string",
          "description" : "Local name (used in certain countries only).",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_code" : {
          "type" : "string",
          "description" : "Code of the customer.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "customer_website" : {
          "type" : "string",
          "description" : "Customer website.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_communication" : {
          "type" : "array",
          "description" : "Contact information, such as phone, e-mail, etc.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/ContactInformation"
          },
          "maxItems" : 20,
          "minItems" : 0
        },
        "customer_vat_id" : {
          "type" : "string",
          "description" : "Customer VAT ID.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_custom_fields" : {
          "description" : "Map of custom fields.",
          "$ref" : "#/definitions/CustomFields"
        },
        "customer_id" : {
          "type" : "string",
          "description" : "Unique identifier of the customer in UUID format."
        },
        "customer_addresses" : {
          "type" : "array",
          "description" : "Address of the customer. Customer can have more than one address.",
          "items" : {
            "$ref" : "#/definitions/Address"
          }
        }
      }
    },
    "CustomerCreate" : {
      "type" : "object",
      "required" : [ "customer_addresses", "customer_code", "customer_communication", "customer_id", "customer_name", "customer_vat_id", "customer_website" ],
      "properties" : {
        "customer_name" : {
          "type" : "string",
          "description" : "International customer name.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_alternative_name" : {
          "type" : "string",
          "description" : "Local name (used in certain countries only).",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_code" : {
          "type" : "string",
          "description" : "Code of the customer.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "customer_website" : {
          "type" : "string",
          "description" : "Customer website.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_communication" : {
          "type" : "array",
          "description" : "Contact information, such as phone, e-mail, etc.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/ContactInformation"
          },
          "maxItems" : 20,
          "minItems" : 0
        },
        "customer_vat_id" : {
          "type" : "string",
          "description" : "Customer VAT ID.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_custom_fields" : {
          "description" : "Map of custom fields.",
          "$ref" : "#/definitions/CustomFields"
        },
        "customer_id" : {
          "type" : "string",
          "description" : "Unique identifier of the customer in UUID format."
        },
        "customer_addresses" : {
          "type" : "array",
          "description" : "Address of the customer. Customer can have more than one address.",
          "items" : {
            "$ref" : "#/definitions/Address"
          }
        }
      }
    },
    "CustomerUpdate" : {
      "type" : "object",
      "required" : [ "customer_addresses", "customer_code", "customer_communication", "customer_name", "customer_vat_id", "customer_website" ],
      "properties" : {
        "customer_name" : {
          "type" : "string",
          "description" : "International customer name.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_alternative_name" : {
          "type" : "string",
          "description" : "Local name (used in certain countries only).",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_code" : {
          "type" : "string",
          "description" : "Code of the customer.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "customer_website" : {
          "type" : "string",
          "description" : "Customer website.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_communication" : {
          "type" : "array",
          "description" : "Contact information, such as phone, e-mail, etc.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/ContactInformation"
          },
          "maxItems" : 20,
          "minItems" : 0
        },
        "customer_vat_id" : {
          "type" : "string",
          "description" : "Customer VAT ID.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "customer_custom_fields" : {
          "description" : "Map of custom fields.",
          "$ref" : "#/definitions/CustomFields"
        },
        "customer_addresses" : {
          "type" : "array",
          "description" : "Address of the customer. Customer can have more than one address. ",
          "items" : {
            "$ref" : "#/definitions/AddressUpdate"
          }
        }
      },
      "description" : "Contains only editable fields."
    },
    "GeoCoordinates" : {
      "type" : "object",
      "required" : [ "latitude", "longitude" ],
      "properties" : {
        "latitude" : {
          "type" : "number",
          "format" : "double",
          "description" : "Latitude of location.",
          "minimum" : -90.0,
          "maximum" : 90.0
        },
        "longitude" : {
          "type" : "number",
          "format" : "double",
          "description" : "Longtitude of location.",
          "minimum" : -180.0,
          "maximum" : 180.0
        },
        "altitude" : {
          "type" : "number",
          "format" : "double",
          "description" : "Altitude of location above sea level (ASL)."
        }
      },
      "description" : "The Geo Coordinate represents the location of a place expressed in latitude, longitude, and altitude."
    },
    "Image" : {
      "type" : "object",
      "required" : [ "description", "uri" ],
      "properties" : {
        "uri" : {
          "type" : "string",
          "description" : "URL of the image resource."
        },
        "description" : {
          "type" : "array",
          "description" : "Description of image for use in alt attribute.",
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "caption" : {
          "type" : "array",
          "description" : "Caption for image.",
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "width" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Width of image in pixels.",
          "minimum" : 0.0
        },
        "height" : {
          "type" : "integer",
          "format" : "int32",
          "description" : "Width of image in pixels.",
          "minimum" : 0.0
        }
      }
    },
    "LocalizedText" : {
      "type" : "object",
      "required" : [ "language", "text" ],
      "properties" : {
        "language" : {
          "type" : "string",
          "description" : "Language of the text."
        },
        "text" : {
          "type" : "string",
          "description" : "The localized text in the given language."
        }
      },
      "description" : "Localized text allows to localize texts throughout the system. It's an array of the texts with specification of the language."
    },
    "Property" : {
      "type" : "object",
      "required" : [ "property_address", "property_attributes", "property_code", "property_contact_information", "property_description", "property_geo_coordinates", "property_id", "property_images", "property_name", "property_room_floors", "property_type", "property_website" ],
      "properties" : {
        "property_name" : {
          "type" : "string",
          "description" : "Formal name of the property.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "property_alternative_name" : {
          "type" : "string",
          "description" : "Name of the property in the local language.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "property_code" : {
          "type" : "string",
          "example" : "CZPRGMARCPA",
          "description" : "Code of the property.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "property_geo_coordinates" : {
          "description" : "Geographic coordinates of the property.",
          "$ref" : "#/definitions/GeoCoordinates"
        },
        "property_description" : {
          "type" : "array",
          "description" : "Short description of the property.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/LocalizedText"
          }
        },
        "property_website" : {
          "type" : "string",
          "description" : "Property website.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "property_contact_information" : {
          "type" : "array",
          "description" : "Contact Information of the property.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/ContactInformation"
          }
        },
        "property_type" : {
          "type" : "string",
          "description" : "Segment category code according to Open Travel (SEG).",
          "enum" : [ "all_suite", "budget", "corporate_business_transient", "deluxe", "economy", "extended_stay", "luxury", "meeting_convention", "moderate", "residential_apartment", "resort", "tourist", "upscale", "efficiency", "standard", "midscale", "quality" ]
        },
        "property_check-in_possible_form" : {
          "type" : "string",
          "example" : "10:00",
          "description" : "Time when check-in is possible, descriptive information.",
          "pattern" : "^([01]\\d|2[0-3]):[0-5]\\d"
        },
        "property_check-out_possible_to" : {
          "type" : "string",
          "example" : "12:00",
          "description" : "Time when check-out is possible, just descriptive information.",
          "pattern" : "^([01]\\d|2[0-3]):[0-5]\\d"
        },
        "property_contact_list" : {
          "type" : "array",
          "description" : "List of contacts for other hotels, taxi services, doctors, and other contacts useful for the guests.",
          "items" : {
            "$ref" : "#/definitions/ContactItem"
          }
        },
        "property_images" : {
          "type" : "array",
          "description" : "Links to the images of the property - multilingual.",
          "items" : {
            "$ref" : "#/definitions/Image"
          },
          "maxItems" : 2147483647,
          "minItems" : 1
        },
        "property_attributes" : {
          "type" : "array",
          "description" : "Attributes of the given property.",
          "uniqueItems" : true,
          "items" : {
            "$ref" : "#/definitions/Attribute"
          }
        },
        "custom_fields" : {
          "description" : "Map of custom fields.",
          "$ref" : "#/definitions/CustomFields"
        },
        "property_room_floors" : {
          "type" : "array",
          "description" : "Definition of the room floors in the property.",
          "items" : {
            "type" : "string"
          }
        },
        "property_id" : {
          "type" : "string",
          "description" : "Unique identifier of the property in UUID format."
        },
        "property_address" : {
          "description" : "Address of the property.",
          "$ref" : "#/definitions/Address"
        }
      }
    }
  }
}