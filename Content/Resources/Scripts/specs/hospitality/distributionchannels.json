{
  "swagger" : "2.0",
  "info" : {
    "version" : "1.0.0",
    "title" : "Hospitality Configuration - Distribution Channel",
    "contact" : { }
  },
  "host" : "rg01we-dp-dev-app5.westeurope.cloudapp.azure.com",
  "basePath" : "/v1",
  "tags" : [ {
    "name" : "distribution_channels"
  } ],
  "schemes" : [ "https" ],
  "paths" : {
    "/hospitality/configuration/distribution_channels" : {
      "get" : {
        "tags" : [ "distribution_channels" ],
        "summary" : "",
        "description" : "Returns a collection of distribution channels.",
        "operationId" : "getDistributionChannels",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "filter",
          "in" : "query",
          "description" : "Ability to filter collection to certain values, it uses FIQL syntax. Example: ?filter=name==snapshot",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort",
          "in" : "query",
          "description" : "Ascending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "sort_desc",
          "in" : "query",
          "description" : "Descending sort parameter where the value is the object attribute.",
          "required" : false,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "Specifies how many results is returned in response. The default value is 50, max allowed is 200. The response can return all results within single call (there is no cursor paging available).",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        }, {
          "name" : "cursor",
          "in" : "query",
          "description" : "Starting paging cursor.",
          "required" : false,
          "type" : "integer",
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "Collection of distribution channels.",
            "schema" : {
              "type" : "array",
              "items" : {
                "$ref" : "#/definitions/DistributionChannel"
              }
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "post" : {
        "tags" : [ "distribution_channels" ],
        "summary" : "",
        "description" : "Creates a new distribution channel.",
        "operationId" : "addDistributionChannel",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "in" : "body",
          "name" : "body",
          "description" : "New distribution channel object.",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/DistributionChannelCreate"
          }
        } ],
        "responses" : {
          "201" : {
            "description" : "The created distribution channel object.",
            "schema" : {
              "$ref" : "#/definitions/DistributionChannel"
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    },
    "/hospitality/configuration/distribution_channels/{distribution_channel_id}" : {
      "get" : {
        "tags" : [ "distribution_channels" ],
        "summary" : "",
        "description" : "Returns a single distribution channel.",
        "operationId" : "getDistributionChannel",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "distribution_channel_id",
          "in" : "path",
          "description" : "Unique identifier of the distribution channel in UUID format.",
          "required" : true,
          "type" : "string"
        } ],
        "responses" : {
          "200" : {
            "description" : "Single distribution channel object.",
            "schema" : {
              "$ref" : "#/definitions/DistributionChannel"
            }
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      },
      "post" : {
        "tags" : [ "distribution_channels" ],
        "summary" : "",
        "description" : "Updates a distribution channel.",
        "operationId" : "updateDistributionChannel",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "distribution_channel_id",
          "in" : "path",
          "description" : "Unique identifier of the distribution channel in UUID format.",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "If-Match",
          "in" : "header",
          "description" : "The value taken from Etag HTTP header (specifies the version of the object). It is used by server to validate that client has actual version.",
          "required" : true,
          "type" : "string",
          "default" : ""
        }, {
          "in" : "body",
          "name" : "body",
          "description" : "Updated distribution channel object (include only the fields you want to update).",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/DistributionChannelUpdate"
          }
        } ],
        "responses" : {
          "204" : {
            "description" : "The distribution channel has been updated."
          }
        },
        "security" : [ {
          "api_key" : [ ]
        } ]
      }
    }
  },
  "securityDefinitions" : {
    "api_key" : {
      "type" : "apiKey",
      "name" : "Authorization",
      "in" : "header"
    }
  },
  "definitions" : {
    "ChannelManagerConfiguration" : {
      "type" : "object",
      "properties" : {
        "channelManagerConfiguration" : {
          "type" : "object",
          "additionalProperties" : {
            "type" : "object"
          }
        }
      },
      "description" : "JSON structure which holds information about distribution channel configuration."
    },
    "DateTimeRange" : {
      "type" : "object",
      "required" : [ "from" ],
      "properties" : {
        "from" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Lower limit of given range."
        },
        "to" : {
          "type" : "string",
          "format" : "date-time",
          "description" : "Upper limit of given range."
        }
      },
      "description" : "Range of dates (also contains time)."
    },
    "DistributionChannel" : {
      "type" : "object",
      "required" : [ "channel_can_send_reservation_notification", "channel_code", "channel_id", "channel_name", "channel_type", "channel_validity" ],
      "properties" : {
        "channel_name" : {
          "type" : "string",
          "description" : "Name of the distribution channel.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "channel_code" : {
          "type" : "string",
          "description" : "Human readable code of the distribution channel.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "channel_type" : {
          "type" : "string",
          "description" : "Type of the channel. For example, Global Distribution System (GDS), Online Travel Agency (OTA), direct booking.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "channel_validity" : {
          "description" : "Range of dates the distribution channel is valid and available for use.",
          "$ref" : "#/definitions/DateTimeRange"
        },
        "channel_can_send_reservation_notification" : {
          "type" : "boolean",
          "description" : "If true, the distribution channel sends reservation notification, it does not check for availability.",
          "default" : false
        },
        "channel_allowed_blocks" : {
          "type" : "array",
          "description" : "If the distribution channel is allowed to send reservation notifications, this field specifies the Block where the channel can send the reservations (i.e., from which block to book). If the value is not supplied, the distribution channel books property inventory.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_rate_plans" : {
          "type" : "array",
          "description" : "Allowed rate plans, which can be used by this distribution channel. If empty, there are no restrictions on rate plans.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_room_types" : {
          "type" : "array",
          "description" : "Allowed room types to book using the distribution channel. If empty, the distribution channel can book any room type.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_packages" : {
          "type" : "array",
          "description" : "Allowed packages to book using the distribution channel. If empty, the distribution channel can book any package.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_promotions" : {
          "type" : "array",
          "description" : "Allowed promotions the distribution channel can use. If empty, the distribution channel can use any promotion.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_manager_configuration" : {
          "description" : "JSON structure which holds information about channel manager configuration.",
          "$ref" : "#/definitions/ChannelManagerConfiguration"
        },
        "channel_id" : {
          "type" : "string",
          "description" : "Unique identifier of the channel."
        }
      }
    },
    "DistributionChannelCreate" : {
      "type" : "object",
      "required" : [ "channel_can_send_reservation_notification", "channel_code", "channel_id", "channel_name", "channel_type", "channel_validity" ],
      "properties" : {
        "channel_name" : {
          "type" : "string",
          "description" : "Name of the distribution channel.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "channel_code" : {
          "type" : "string",
          "description" : "Human readable code of the distribution channel.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "channel_type" : {
          "type" : "string",
          "description" : "Type of the channel. For example, Global Distribution System (GDS), Online Travel Agency (OTA), direct booking.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "channel_validity" : {
          "description" : "Range of dates the distribution channel is valid and available for use.",
          "$ref" : "#/definitions/DateTimeRange"
        },
        "channel_can_send_reservation_notification" : {
          "type" : "boolean",
          "description" : "If true, the distribution channel sends reservation notification, it does not check for availability.",
          "default" : false
        },
        "channel_allowed_blocks" : {
          "type" : "array",
          "description" : "If the distribution channel is allowed to send reservation notifications, this field specifies the Block where the channel can send the reservations (i.e., from which block to book). If the value is not supplied, the distribution channel books property inventory.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_rate_plans" : {
          "type" : "array",
          "description" : "Allowed rate plans, which can be used by this distribution channel. If empty, there are no restrictions on rate plans.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_room_types" : {
          "type" : "array",
          "description" : "Allowed room types to book using the distribution channel. If empty, the distribution channel can book any room type.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_packages" : {
          "type" : "array",
          "description" : "Allowed packages to book using the distribution channel. If empty, the distribution channel can book any package.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_promotions" : {
          "type" : "array",
          "description" : "Allowed promotions the distribution channel can use. If empty, the distribution channel can use any promotion.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_manager_configuration" : {
          "description" : "JSON structure which holds information about channel manager configuration.",
          "$ref" : "#/definitions/ChannelManagerConfiguration"
        },
        "channel_id" : {
          "type" : "string",
          "description" : "Unique identifier of the channel."
        }
      }
    },
    "DistributionChannelUpdate" : {
      "type" : "object",
      "required" : [ "channel_can_send_reservation_notification", "channel_code", "channel_name", "channel_type", "channel_validity" ],
      "properties" : {
        "channel_name" : {
          "type" : "string",
          "description" : "Name of the distribution channel.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "channel_code" : {
          "type" : "string",
          "description" : "Human readable code of the distribution channel.",
          "minLength" : 0,
          "maxLength" : 20
        },
        "channel_type" : {
          "type" : "string",
          "description" : "Type of the channel. For example, Global Distribution System (GDS), Online Travel Agency (OTA), direct booking.",
          "minLength" : 0,
          "maxLength" : 255
        },
        "channel_validity" : {
          "description" : "Range of dates the distribution channel is valid and available for use.",
          "$ref" : "#/definitions/DateTimeRange"
        },
        "channel_can_send_reservation_notification" : {
          "type" : "boolean",
          "description" : "If true, the distribution channel sends reservation notification, it does not check for availability.",
          "default" : false
        },
        "channel_allowed_blocks" : {
          "type" : "array",
          "description" : "If the distribution channel is allowed to send reservation notifications, this field specifies the Block where the channel can send the reservations (i.e., from which block to book). If the value is not supplied, the distribution channel books property inventory.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_rate_plans" : {
          "type" : "array",
          "description" : "Allowed rate plans, which can be used by this distribution channel. If empty, there are no restrictions on rate plans.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_room_types" : {
          "type" : "array",
          "description" : "Allowed room types to book using the distribution channel. If empty, the distribution channel can book any room type.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_packages" : {
          "type" : "array",
          "description" : "Allowed packages to book using the distribution channel. If empty, the distribution channel can book any package.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_allowed_promotions" : {
          "type" : "array",
          "description" : "Allowed promotions the distribution channel can use. If empty, the distribution channel can use any promotion.",
          "items" : {
            "$ref" : "#/definitions/ReferenceWithDateTimeRange"
          }
        },
        "channel_manager_configuration" : {
          "description" : "JSON structure which holds information about channel manager configuration.",
          "$ref" : "#/definitions/ChannelManagerConfiguration"
        }
      }
    },
    "Reference" : {
      "type" : "object",
      "required" : [ "id", "module" ],
      "properties" : {
        "module" : {
          "type" : "string",
          "description" : "Module name."
        },
        "id" : {
          "type" : "string",
          "description" : "Identifier of referenced entity."
        }
      },
      "description" : "Reference to some other entity (sub-types)."
    },
    "ReferenceWithDateTimeRange" : {
      "type" : "object",
      "required" : [ "booking_date_time", "reference" ],
      "properties" : {
        "reference" : {
          "description" : "Reference to the rate plan, room type etc.",
          "$ref" : "#/definitions/Reference"
        },
        "booking_date_time" : {
          "description" : "Date time range for which is the given rate plan or room type allowed to use.",
          "$ref" : "#/definitions/DateTimeRange"
        }
      }
    }
  }
}