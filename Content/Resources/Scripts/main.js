/* make operation headings collapsible */
$(function() {
  $('section.endpoint h3').click(function(){
    var header = $(this);
    var content = header.next('div');
    if (content.is(':visible')) {
      header.removeClass('active');
      content.slideUp();
    }
    else {
      header.addClass('active');
      content.slideDown();
    }
  })
});

/* fix page markup, that cannot be avoided */
$(function() {
	$('form.search').addClass('navbar-form navbar-right')
});
